(function(){

var getLocation = function() {
  return new Promise(function(resolve, reject) {
    navigator.geolocation.getCurrentPosition(function(position) {
      resolve(position);
    });
  });
};

var getTimes = function(position) {
  return new Promise(function(resolve, reject) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var current_datetime = new Date();
      resolve(SunCalc.getTimes(current_datetime, position.coords.latitude, position.coords.longitude));
    });
  });
};

var updateTimeTable = function(times, elements) {
  elements.sunrise_time_el.innerHTML = times.sunrise.toLocaleTimeString();
  elements.sunset_time_el.innerHTML = times.sunset.toLocaleTimeString();
  elements.solnoon_time_el.innerHTML = times.solarNoon.toLocaleTimeString();
};

var drawDayNightArcs = function(sunrise, sunset, elements) {
  var sunrise_seconds_since_midnight = sunrise.getSeconds() + (60 * sunrise.getMinutes()) + (60 * 60 * sunrise.getHours()),
      sunset_seconds_since_midnight = sunset.getSeconds() + (60 * sunset.getMinutes()) + (60 * 60 * sunset.getHours()),
      sunrise_degrees = sunrise_seconds_since_midnight / 86400 * 360,
      sunset_degrees = sunset_seconds_since_midnight / 86400 * 360,

      // Rotate the arc so that midnight starts at the bottom of the day circle.
      sunrise_degrees = sunrise_degrees + 180,
      sunset_degrees = sunset_degrees + 180;

  // Draw the daytime arc (actually a pie wedge).
  elements.daytime_el.setAttribute('d', describeArc(200, 200, 190, sunrise_degrees, sunset_degrees));

  // Draw the nighttime arc.
  elements.nighttime_el.setAttribute('d', describeArc(200, 200, 190, sunset_degrees, sunrise_degrees));
};

var placeSolarNoon = function(solarNoon, elements) {
  // Solar noon
  var solnoon_seconds_since_midnight = solarNoon.getSeconds() + (60 * solarNoon.getMinutes()) + (60 * 60 * solarNoon.getHours()),
      solnoon_degrees = solnoon_seconds_since_midnight / 86400 * 360,
      solar_noon_location = polarToCartesian(200, 200, 190, solnoon_degrees + 180);

  elements.sun_noon_el.setAttribute('cx', solar_noon_location.x);
  elements.sun_noon_el.setAttribute('cy', solar_noon_location.y);

  elements.sun_noon_label_el.setAttribute('x', solar_noon_location.x + 16);
  elements.sun_noon_label_el.setAttribute('y', solar_noon_location.y + 2);
};

var rotateHourhand = function(rotate_degrees, elements) {
  elements.hourhand_el.setAttribute('transform', 'rotate(' + rotate_degrees + ' 200 200)')
  elements.hourarc_el.setAttribute('d', describeArc(200, 200, 165, 180, rotate_degrees + 180));
};

var updateCurrentTime = function(d, elements) {
  elements.current_time_el.innerHTML = d.toLocaleTimeString();
};

var placeSun = function(rotate_degrees, elements) {
  var time_arc_location = polarToCartesian(200, 200, 190, rotate_degrees + 180);
  elements.sun_el.setAttribute('cx', time_arc_location.x);
  elements.sun_el.setAttribute('cy', time_arc_location.y);
};

// http://stackoverflow.com/a/18473154
var polarToCartesian = function(centerX, centerY, radius, angleInDegrees) {
  var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;

  return {
    x: centerX + (radius * Math.cos(angleInRadians)),
    y: centerY + (radius * Math.sin(angleInRadians))
  };
};
var describeArc = function(x, y, radius, startAngle, endAngle){
  var start = polarToCartesian(x, y, radius, endAngle);
  var end = polarToCartesian(x, y, radius, startAngle);
  var largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";
  var d = [
      "M", start.x, start.y,
      "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y,
      "L", x,y,
      "L", start.x, start.y
  ].join(" ");

  return d;
};

if ("geolocation" in navigator && true) {
  document.addEventListener("DOMContentLoaded", function() {
    var elements = {
      current_time_el: document.getElementById("current-time"),
      sunrise_time_el: document.getElementById("sunrise-time"),
      sunset_time_el: document.getElementById("sunset-time"),
      solnoon_time_el: document.getElementById("solar-noon-time"),
      daytime_el: document.getElementById('daytime'),
      nighttime_el: document.getElementById('nighttime'),
      sun_noon_el: document.getElementById('sun-noon'),
      sun_noon_label_el: document.getElementById('sun-noon-label'),
      hourarc_el: document.getElementById('hourarc'),
      hourhand_el: document.getElementById('hourhand'),
      sun_el: document.getElementById('sun')
    };

    // Get the current location.
    getLocation()
      .then(function(position) {
        // Get interesting sun times for the returned position.
        return getTimes(position);
      })
      .then(function(times) {
        // Draw day and night arcs.
        updateTimeTable(times, elements);
        drawDayNightArcs(times.sunrise, times.sunset, elements);

        // Place the solar noon.
        placeSolarNoon(times.solarNoon, elements);
      });

    // Start the hour hand and sun movements.
    setInterval(function() {
      var d = new Date(),
          seconds_since_midnight = d.getSeconds() + (60 * d.getMinutes()) + (60 * 60 * d.getHours()),
          rotate_degrees = seconds_since_midnight / 86400 * 360;

      updateCurrentTime(d, elements);
      rotateHourhand(rotate_degrees, elements);

      // Put the sun at the current time.
      placeSun(rotate_degrees, elements);
    }, 1000)
  });
}
else {
  alert('Your browser does not support geolocation.');
}

})(document, window, SunCalc);
