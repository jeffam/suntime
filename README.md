# Suntime

This single page app displays the time in a 24 hour 'clock' that shows the current time in the context of the daily sunlight phase.

https://gitlab.com/jeffam/suntime

## Roadmap

- Clean up code.
    - Consider moving to webpack with livereloading. See https://www.sitepoint.com/beginners-guide-to-webpack-2-and-module-bundling/
    - Improve timers
        - Update hour hand every minute _on_ the minute, using a self-adjusting timer. See http://stackoverflow.com/a/29972322
    - Add Promises polyfill for IE
- Host somewhere for free. Perhaps [gitlab pages][]
    - Use TLS. iOS 10.1 won't do geolocation without it. Gitlab may bake Let's Encrypt in automatically very soon. See https://gitlab.com/gitlab-org/gitlab-ce/issues/28996. Even without that automation, a helper script exists: https://github.com/rolodato/gitlab-letsencrypt. This could be used as an npm script pretty easily.
- Improve installation process on iOS and Android.

## Notes

Thanks to the awesome [SunCalc Library][]. Using `npm` to grab the library and [browserify][] to make it available to the app. Maybe a little kludgy.

Uses the [Geolocation API][].

To update the SunCalc lib:

```
$ npm update
$ npm run build   # see `scripts` in package.json
```

[gitlab pages]: https://pages.gitlab.io/
[SunCalc Library]: https://github.com/mourner/suncalc
[browserify]: https://github.com/substack/node-browserify
[Geolocation API]: https://developer.mozilla.org/en-US/docs/Web/API/Geolocation
